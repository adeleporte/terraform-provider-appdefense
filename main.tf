provider "appdefense" {
  host      = "appdefense-live.vmware.com"
  key       = ""

}

data "appdefense_endpoint" "vm1"{
  name      = "MyVCNAppL2-web"
}

data "appdefense_endpoint" "vm2"{
  name      = "MyVCNAppL2-app"
}


resource "appdefense_service" "service1" {
  name  = "service1"

  member {
    id  = "${data.appdefense_endpoint.vm1.id}"
    name = "${data.appdefense_endpoint.vm1.name}"
  }

  member {
    id  = "${data.appdefense_endpoint.vm2.id}"
    name = "${data.appdefense_endpoint.vm2.name}"
  }

}

output "output1" {
  value = "${data.appdefense_endpoint.vm1.id}"
}


