package appdefense

import (
	//"fmt"
	"context"
	"log"

	"github.com/hashicorp/terraform/helper/schema"
	api "github.com/vmware/go-vmware-appdefense"
)

func resourceService() *schema.Resource {
	return &schema.Resource{
		Create: resourceServiceCreate,
		Read:   resourceServiceRead,
		Update: resourceServiceUpdate,
		Delete: resourceServiceDelete,
		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"member": getMembersSchema(),
		},
	}
}

func getMembersSchema() *schema.Schema {
	return &schema.Schema{
		Type:        schema.TypeList,
		Description: "List of members",
		Optional:    true,
		Elem: &schema.Resource{
			Schema: map[string]*schema.Schema{
				"id": &schema.Schema{
					Type:     schema.TypeInt,
					Required: true,
				},
				"name": &schema.Schema{
					Type:     schema.TypeString,
					Required: true,
				},
			},
		},
	}
}

func getMembersFromSchema(d *schema.ResourceData) []api.Endpoint {
	members := d.Get("member").([]interface{})

	var memberList []api.Endpoint
	for _, member := range members {
		data := member.(map[string]interface{})
		elem := api.Endpoint{
			Name: data["name"].(string),
			Id:   data["id"].(int),
		}

		memberList = append(memberList, elem)
	}
	return memberList
}

func resourceServiceRead(d *schema.ResourceData, m interface{}) error {
	log.Println("*********************************************** EndPoint READ*****************************************************************")

	client := m.(*api.APIClient)

	// Get Service
	service, _, _ := client.EndPointApi.GetService(context.Background())
	println(service.ProvisionedBy)
	d.SetId(service.Name)

	return nil
}

func resourceServiceUpdate(d *schema.ResourceData, m interface{}) error {

	return resourceServiceCreate(d, m)
}

func resourceServiceDelete(d *schema.ResourceData, m interface{}) error {

	client := m.(*api.APIClient)

	// Get Service
	service, _, _ := client.EndPointApi.GetService(context.Background())
	println(service.ProvisionedBy)

	endpointsDeleted := []api.Endpoint{}


	results3, _, _ := client.EndPointApi.SetService(context.Background(), service, endpointsDeleted)
	println(results3.ProvisionedBy)

	d.SetId("")
	return resourceServiceRead(d, m)
}

func resourceServiceCreate(d *schema.ResourceData, m interface{}) error {

	client := m.(*api.APIClient)

	// Get Service
	service, _, _ := client.EndPointApi.GetService(context.Background())
	println(service.ProvisionedBy)
	d.SetId(service.Name)

	// Add members
	members := getMembersFromSchema(d)
	var endpointsFiltered []api.Endpoint

	endpoints, _, _ := client.EndPointApi.GetEndpoints(context.Background())

	println("###########################################################################")

	for _, member := range members {
		println(member.Name)

		for _, endpoint := range endpoints {
			println(endpoint.Name)

			if member.Name == endpoint.Name {
				println("------------------------match")
				println(endpoint.Id)
				println(endpoint.Name)

				//d.SetId(fmt.Sprint(endpoint.Id))
				//d.Set("name", endpoint.Name)
				//d.Set("ipaddress", endpoint.IpAddress)
				//d.Set("createat", endpoint.CreatedAt)

				endpointsFiltered = append(endpointsFiltered, endpoint)
			}
		}
	}



	results3, _, _ := client.EndPointApi.SetService(context.Background(), service, endpointsFiltered)
	println(results3.ProvisionedBy)

	return resourceServiceRead(d, m)
}
