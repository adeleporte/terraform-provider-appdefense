package appdefense

import (
	//"fmt"

	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/terraform"
	api "github.com/vmware/go-vmware-appdefense"
	//"context"
)

// Provider provide some functions
func Provider() terraform.ResourceProvider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"host": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("appdefense_HOST", nil),
			},
			"key": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("appdefense_KEY", nil),
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"appdefense_service":      resourceService(),
		},

		DataSourcesMap: map[string]*schema.Resource{
			"appdefense_endpoint": dataSourceEndPoint(),
		},

		ConfigureFunc: providerConfigure,
	}
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	host := d.Get("host").(string)
	key := d.Get("key").(string)

	print(host)
	print(key)

	cfg := api.Configuration{
		BasePath:  "https://" + host,
		Host:      host,
		Scheme:    "https",
		UserAgent: "terraform-provider-appdefense/1.0",
		Key:       key,
	}

	client := api.NewAPIClient(&cfg)
	/*
		auth := api.UserCredential{
			Username: username,
			Password: password,
			Domain: &api.Domain{
				DomainType: "LOCAL",
			},
		}
		log.Println(auth)
	*/
	/*
		// Connect and get token

		token, resp, err := client.AuthenticationApi.Create(context.Background(), auth)
		log.Print("******************************appdefense PROVIDER INIT***********************************")
		log.Println(token)
		log.Println(resp)
		log.Println(err)
	*/

	/*
		auth := velocloud.AuthObject{
			Username: username,
			Password: password,
		}

		// Connect and Get Cookie
		resp, err := client.LoginApi.LoginOperatorLogin(context.Background(), auth)
		if err != nil {
			return fmt.Println("Velocloud provider connectivity check failed")
		}
		log.Println(resp)
		client.AddCookie(string(resp.Cookies()[1].Raw))
	*/
	return client, nil
}
