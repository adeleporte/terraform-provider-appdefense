package appdefense

import (
	//"fmt"
	"context"
	"fmt"
	"log"

	"github.com/hashicorp/terraform/helper/schema"
	api "github.com/vmware/go-vmware-appdefense"
)

func dataSourceEndPoint() *schema.Resource {
	return &schema.Resource{
		Read: dataSourceEndPointRead,
		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			//"id": &schema.Schema{
			//	Type:     schema.TypeInt,
			//	Computed: true,
			//},
			"createat": &schema.Schema{
				Type:     schema.TypeFloat,
				Computed: true,
			},
			"ipaddress": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func dataSourceEndPointRead(d *schema.ResourceData, m interface{}) error {
	log.Println("*********************************************** EndPoint READ*****************************************************************")

	client := m.(*api.APIClient)
	var endpointsFiltered []api.Endpoint

	endpoints, _, _ := client.EndPointApi.GetEndpoints(context.Background())

	println("###########################################################################")

	for _, endpoint := range endpoints {
		if endpoint.Name == d.Get("name") {
			println(endpoint.Id)
			println(endpoint.Name)
			println(endpoint.IpAddress)

			d.SetId(fmt.Sprint(endpoint.Id))
			//d.Set("name", endpoint.Name)
			d.Set("ipaddress", endpoint.IpAddress)
			d.Set("createat", endpoint.CreatedAt)

			endpointsFiltered = append(endpointsFiltered, endpoint)
		}
	}

	//service, _, _ := client.EndPointApi.GetService(context.Background())
	//println(service.ProvisionedBy)

	//results3, _, _ := client.EndPointApi.SetService(context.Background(), service, endpoints_filtered)
	//println(results3.ProvisionedBy)

	return nil
}
